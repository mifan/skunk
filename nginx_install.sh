#!/usr/bin/env bash

####################################################
# NOTICE: before run this script, do following steps
# copy nginx init script to /etc/init.d/
# sudo chmod +x /etc/init.d/nginx
# sudo /usr/sbin/update-rc.d -f nginx defaults
####################################################

set -eu

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "*** This script must be run as root ***" 1>&2
   exit 1
fi

# Install dependence:
DEPENDENCE=(
  build-essential
  zlib1g-dev
  libssl-dev
  libreadline-dev
  libgeoip-dev
  libpcre3-dev
)


for dep in ${DEPENDENCE[@]} ; do

  if ! dpkg -s $dep > /dev/null 2>&1 ; then
    echo $dep is installing
    apt-get -y install $dep
    echo $dep was installed
  else
    echo $dep was already installed
  fi

done


## Nginx Build
BUILD_TIME=`/bin/date +%Y%m%d%H%M%S`
RESOURCES_PATH=`pwd`
WORKSPACE=/root/workspace

#nginx
NGINX_CONFIG_FILE=/usr/local/nginx/conf/nginx.conf
NGINX_VERSION=1.4.2
#NGINX_INSTLL_VERSION_LOCATION is just for check if need build nginx
NGINX_INSTLL_VERSION_LOCATION=/usr/local/nginx-$NGINX_VERSION
NGINX_INSTLL_LOCATION=/usr/local/nginx-$NGINX_VERSION-$BUILD_TIME
NGINX_LINK_LOCATION=/usr/local/nginx
NGINX_SOURCE=nginx-$NGINX_VERSION
#nginx_upload_module
NGINX_UPLOAD_MODULE_SOURCE=nginx_upload_module-2.2.0




if [ ! -d $WORKSPACE ] ; then
  mkdir -p $WORKSPACE
fi


clean_nginx_build_env() {
  cd $WORKSPACE
  #clean old directorys if existed
  [[ -d $NGINX_SOURCE ]] && rm -rf $NGINX_SOURCE
  [[ -d $NGINX_UPLOAD_MODULE_SOURCE ]] && rm -rf $NGINX_UPLOAD_MODULE_SOURCE

  #download sources
  if [ ! -f $NGINX_SOURCE.tar.gz ] ; then
    wget http://nginx.org/download/$NGINX_SOURCE.tar.gz
  fi
  if [ ! -f $NGINX_UPLOAD_MODULE_SOURCE.tar.gz ] ; then
    wget http://www.grid.net.ru/nginx/download/$NGINX_UPLOAD_MODULE_SOURCE.tar.gz
  fi
    #extract sources
  tar -xzf $NGINX_SOURCE.tar.gz
  tar -xzf $NGINX_UPLOAD_MODULE_SOURCE.tar.gz
}

build_nginx() {
  cd $WORKSPACE/$NGINX_SOURCE
  ./configure --with-http_geoip_module \
              --with-http_ssl_module \
              --with-http_flv_module \
              --with-http_gzip_static_module \
              --prefix=$NGINX_INSTLL_LOCATION

  make
  make install
  ## additional modules
  #--add-module=$WORKSPACE/$NGINX_UPLOAD_MODULE_SOURCE \
}


config_recycle_nginx() {

  if [ -f $NGINX_CONFIG_FILE ] ; then
    cp -f $NGINX_CONFIG_FILE  $NGINX_INSTLL_LOCATION/conf
    echo "copied nginx config file."
  else
    cp -f $RESOURCES_PATH/nginx/nginx.conf  $NGINX_INSTLL_LOCATION/conf
    echo "copied original nginx config file."
  fi


  if [ ! -f /etc/init.d/nginx ] ; then
    cp -f $RESOURCES_PATH/nginx/nginx  /etc/init.d/nginx
    echo "copied nginx config file."
    chmod +x /etc/init.d/nginx
    /usr/sbin/update-rc.d -f nginx defaults
  fi

  /etc/init.d/nginx stop
  echo "stop nginx service."

  [[ -d $NGINX_LINK_LOCATION ]] && rm -rf $NGINX_LINK_LOCATION
  ln -s  $NGINX_INSTLL_LOCATION $NGINX_LINK_LOCATION
  ln -s  $NGINX_INSTLL_VERSION_LOCATION $NGINX_LINK_LOCATION
  echo "ln nginx to work directory."

  /etc/init.d/nginx start
  echo "start nginx service."

}


if [ ! -d $NGINX_INSTLL_VERSION_LOCATION ] ; then
  clean_nginx_build_env
  build_nginx
  config_recycle_nginx
fi

cd $RESOURCES_PATH
