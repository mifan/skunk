#!/usr/bin/env bash


# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "*** This script must be run as root ***" 1>&2
   exit 1
fi

# Install dependence:
DEPENDENCE=(
  git-core
  build-essential
  zlib1g-dev
  libssl-dev
  libreadline-dev
)

for dep in ${DEPENDENCE[@]} ; do

  if ! dpkg -s $dep > /dev/null 2>&1 ; then
    echo $dep is installing
    apt-get -y install $dep
    echo $dep was installed
  else
    echo $dep was already installed
  fi

done
